from dataclasses import dataclass


@dataclass
class UneValeur:
    valeur: str # pas de valeur par défaut


# pas de valeur par défaut : cette ligne plante
h2 = UneValeur()
