# exemple de base pour déclarer une dataclass

from dataclasses import dataclass


@dataclass
class UneValeur:
    valeur: str


h1 = UneValeur("bonjour")
print(h1)
print(h1.valeur)
