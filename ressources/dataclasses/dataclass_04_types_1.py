from dataclasses import dataclass


@dataclass
class Prix:
    valeur: float
    taxe : float = 0.0


# une dataclass est un type comme un autre, on peut l'utiliser comme on veut
@dataclass
class Panier:
    mes_prix : list[Prix]


prix1 = Prix(10.0, 0.2)
prix2 = Prix(10.0)
prix3 = Prix(valeur=10.0, taxe=0.2)

panier = Panier([prix1, prix2, prix3])

print(panier)
