from dataclasses import dataclass


@dataclass
class UneValeur:
    valeur: str = "hello" # pas de valeur par défaut


# valeur par défaut : cette ligne marche
h2 = UneValeur()
print(h2)
print(h2.valeur)
