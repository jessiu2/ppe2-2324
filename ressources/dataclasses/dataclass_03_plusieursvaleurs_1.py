from dataclasses import dataclass


@dataclass
class Prix:
    valeur: float # pas de valeur par défaut
    taxe : float = 0.0


# différentes façon de créer un prix
prix1 = Prix(10.0, 0.2)
prix2 = Prix(10.0)
prix3 = Prix(valeur=10.0, taxe=0.2) # doivent avoit le même nom que déclaré

print(prix1)
print(prix2)
print(prix3)
