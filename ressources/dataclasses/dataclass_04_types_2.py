from dataclasses import dataclass


@dataclass
class Prix:
    valeur: float
    taxe : float = 0.0


@dataclass
class Panier:
    mes_prix : list[Prix]


# on peut définir des fonctions qui utilisent les dataclasses

def prix_ht(p : Prix):
    return p.valeur


def prix_ttc(p : Prix):
    return p.valeur * (1 + p.taxe)


def panier_ttc(panier : Panier):
    return sum([prix_ttc(p) for p in panier.mes_prix])



prix1 = Prix(10.0, 0.2)
prix2 = Prix(10.0)
prix3 = Prix(valeur=10.0, taxe=0.2)

panier = Panier([prix1, prix2, prix3])

print(prix_ht(prix1))
print(prix_ttc(prix1))

print(panier_ttc(panier))
