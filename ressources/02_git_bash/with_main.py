print('Je suis toujours lancé')

# Vrai uniquement si on lance depuis un terminal.
# Utilisé quand on veut exécuter ET importer un script.
#
# Indicateur clair d'intention : toujours l'utiliser !
if __name__ == '__main__':
    print('Je suis lancé depuis un terminal,')
    print("autrement, cette portion ne s'exécute pas.")
