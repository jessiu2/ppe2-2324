import sys # accès à l'entrée standard (entre autres)

une = next(sys.stdin) # lire une ligne (avec CR/LF)
tout = sys.stdin.read() # lire tout d'un seul coup

for line in sys.stdin: # on itère ligne par ligne
    [...]

# on utilise pas toutes ces méthodes en même temps,
# on utilise ce qui convient à la situation