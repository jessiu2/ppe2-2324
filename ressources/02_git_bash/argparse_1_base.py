import argparse # pour parser la ligne de commande

# le parser auquel on ajoutera arguments et options
my_parser = argparse.ArgumentParser()

# [...] configuration du parser

# on parse la ligne de commande
#    *APRÈS* configuration de "my_parser"
#    et on récupère le tout de manière structurée
my_args = my_parser.parse_args()

print(my_args)
