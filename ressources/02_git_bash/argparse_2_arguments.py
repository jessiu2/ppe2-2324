import argparse

# le kwarg (keyword argument) "description" permet
# d'ajouter une description quand on demande de l'aide.
my_parser = argparse.ArgumentParser(description="Hi.")

my_parser.add_argument("arg1", help="fails if not given")
my_parser.add_argument(
    "arg2",
    nargs="?",
    help="An optional argument, defaults to None"
)

my_args = my_parser.parse_args()

print(my_args)
print(my_args.arg1)
print(my_args.arg2)
