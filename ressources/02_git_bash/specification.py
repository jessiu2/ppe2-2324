# première façon : fonction qui ne fait rien
# peut indiquer qu'on souhaite la remplir un jour
def fonction1():
    pass


# deuxième façon: fonction qui lève une exception
# "plante" tout de suite
# peut indiquer qu'on ne souhaite pas l'implémenter
def fonction2():
    raise NotImplementedError()
