import argparse

my_parser = argparse.ArgumentParser()

# -h et --help sont là par défaut
my_parser.add_argument(
    "-n", # option courte *avant* la longue
    "--number-of-times",
    type=int, # on attend un entier, par défaut str
    default=0,
    help="Le nombre de lignes à afficher"
)
my_parser.add_argument(
    '--greeting',
    choices=('Hello', 'Hi'),
    help="Plante si ce qu'on donne n'est pas un choix."
)

my_args = my_parser.parse_args()

print(my_args)
print(my_args.number_of_times, type(my_args.number_of_times))
print(my_args.greeting)

num = my_args.number_of_times
greet = my_args.greeting

for i in range(my_args.number_of_times):
    print(f"{greet} you!")
